<?php
App::uses('AppModel', 'Model');
class OpauthSetting extends AppModel {

	public $actsAs = array(
		'App.Expandable' => array(
            'with' => 'OpauthSettingExpanded',
			'encode_json' => true
		)
	);

	public $hasMany = array(
		'OpauthSettingExpanded' => array(
			'className' => 'Opauth.OpauthSettingExpanded',
			'dependent' => true
		)
	);
}
