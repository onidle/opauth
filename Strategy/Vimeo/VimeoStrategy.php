<?php
/**
 * Vimeo strategy for Opauth
 *
 * More information on Opauth: http://opauth.org
 *
 * Class: VimeoStrategy
 *
 * Vimeo strategy for Opauth
 * based on https://developer.vimeo.com/api/authentication
 *
 * @package	  Opauth.Vimeo
 * @author    Ondrej Nedvidek <ondra@on-idle.com>
 * @copyright 2017 on-IDLE Ltd. <http://www.on-idle.com>
 * @link      https://www.on-idle.com
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 *
 * @see OpauthStrategy
 */
class VimeoStrategy extends OpauthStrategy{
	
	/**
	 * Compulsory config keys, listed as unassociative arrays
	 */
	public $expects = array('client_id', 'client_secret');
	
	/**
	 * Optional config keys, without predefining any default values.
	 */
	public $optionals = array('redirect_uri', 'scope', 'state', 'access_type', 'approval_prompt');
	
	/**
	 * Optional config keys with respective default values, listed as associative arrays
	 * eg. array('scope' => 'email');
	 */
	public $defaults = array(
		'redirect_uri' => '{complete_url_to_strategy}oauth2callback',
		// 'scope' => 'public' // (optional)
	);
	
	/**
	 * Auth request
	 */
	public function request(){
		$url = 'https://api.vimeo.com/oauth/authorize';
		$params = array(
			'client_id' => $this->strategy['client_id'],
			'redirect_uri' => $this->strategy['redirect_uri'],
			'response_type' => 'code',
		);

		foreach ($this->optionals as $key){
			if (!empty($this->strategy[$key])) $params[$key] = $this->strategy[$key];
		}
		
		$this->clientGet($url, $params);
	}
	
	/**
	 * Internal callback, after OAuth
	 */
	public function oauth2callback(){
		if (array_key_exists('code', $_GET) && !empty($_GET['code'])){
			$code = $_GET['code'];
			$url = 'https://api.vimeo.com/oauth/access_token';
			$params = array(
				'grant_type' => 'authorization_code',
				'code' => $code,
				'redirect_uri' => $this->strategy['redirect_uri'],
			);
            $options['http']['header'] = 
                "Authorization: basic " . base64_encode($this->strategy['client_id'] . ":" . $this->strategy['client_secret']);
            $options['http']['header'] .= "\r\nContent-type: application/x-www-form-urlencoded";
			$response = $this->serverPost($url, $params, $options, $headers);
			
			$results = json_decode($response);
			
			if (!empty($results) && !empty($results->access_token)){
				
				$this->auth = array(
					'uid' => $results->user->name,
					'info' => array(),
					'credentials' => array(
						'token' => $results->access_token,
					),
					'raw' => json_encode($results->user)
				);

				if (!empty($results->refresh_token))
				{
					$this->auth['credentials']['refresh_token'] = $results->refresh_token;
				}
				
				$this->callback();
			}
			else{
				$error = array(
					'code' => 'access_token_error',
					'message' => 'Failed when attempting to obtain access token',
					'raw' => array(
						'response' => $response,
						'headers' => $headers
					)
				);

				$this->errorCallback($error);
			}
		}
		else{
			$error = array(
				'code' => 'oauth2callback_error',
				'raw' => $_GET
			);
			
			$this->errorCallback($error);
		}
	}
}
